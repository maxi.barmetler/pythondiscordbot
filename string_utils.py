import string
import re


def indent(x, spaces):
    return '\n'.join([' ' * spaces + line for line in x.split('\n')])


def indented_replace(x, old, new) -> str:
    sections = re.split(f'{re.escape(old)}', x)
    for i in range(len(sections) - 1):
        m = re.search(r' +$', sections[i])
        indentation = 0
        if m:
            start, end = m.span()
            indentation = (end - start)
        sections[i] += indent(new, indentation).lstrip()
    return ''.join(sections)


def indented_replace_dict(x, d: dict, max_depth = 4) -> str:
    if not max_depth:
        return x

    for key in sorted(d, key = lambda _x: len(_x), reverse=True):
        if key in x:
            new = indented_replace_dict(d[key], d, max_depth - 1)
            x = indented_replace(x, key, new)

    return x


def main():
    placeholder = '@placeholder'

    code = (
        'def fun():\n'
        '  for i in range(10):\n'
        f'    {placeholder}\n'
        '  return \'Hello World!\'\n'
        'print(fun())'
    )

    snippet=(
        'for j in range(2):\n'
        '  print(f\'Lol {j}\')'
    )

    result=indented_replace(code, placeholder, snippet)

    print(f'Old:\n{code}\n====================')
    print(f'Replace \'{placeholder}\' with:\n{snippet}\n====================')
    print(f'Result:\n{result}\n====================')

    exec(result)


if __name__ == '__main__':
    main()
