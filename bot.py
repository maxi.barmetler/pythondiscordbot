import contextlib
import json
import os
import re
import sys
import threading
from io import StringIO

import discord
import mysql.connector
from func_timeout import FunctionTimedOut, func_timeout
from mysql.connector import Error

import config
import doc
import string_utils

client = discord.Client()

# catch stdout when running exec command
@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old


data_base = {}

mods = ['Administrator', 'Admin', 'Moderator']
admins = ['MaxiBarometer#3826']
# admins = []

allowed_imports = [
    'random',
    'numpy',
    'math',
    'string',
    'time'
]

alternate_imports = [
    'links',
    'requests'
]


def help(arg=None):
    if arg == None:
        return (
            '`Commands:`\n'
            '```{}```'
        ).format(
            '\n'.join(
                [v for key in doc.cmd_desc
                    for v in doc.cmd_desc[key]]
            )
        )
    else:
        if arg in doc.cmd_desc:
            return'```{}```'.format('\n'.join(
                [v for v in doc.cmd_desc[arg]]
            ))
        if arg in doc.exec_desc:
            return'```{}```'.format('\n'.join(
                [v for v in doc.exec_desc[arg]]
            ))
        return f'Command "{arg}" not found'


def connect_db():
    try:
        return mysql.connector.connect(
            user=data_base['db_user'],
            password=data_base['db_password'],
            host=data_base['db_host'],
            database=data_base['db_database']
        )
    except Error as e:
        print(e)
        return None


def query_db(query: str, fetch=False, data=None, verbose=False):
    if verbose:
        print(f'Query:\n{query}')
    connection = connect_db()
    cursor = connection.cursor()
    ret = None
    try:
        if data != None:
            cursor.execute(query, data)
        else:
            cursor.execute(query)
        if fetch:
            ret = cursor.fetchall()
    except Error as e:
        print(e)
    connection.commit()
    connection.close()
    return ret


async def reply_msg(message: discord.Message, reply: str):
    return await message.channel.send(f'{message.author.mention}\n{reply}')


def eval_secure(code: str, timeout=16):
    import random
    import numpy
    import math
    try:
        return func_timeout(timeout, lambda: eval(code, {'random': random, 'numpy': numpy, 'math': math}))
    except TimeoutError:
        print('Timed out!')
        return None


def exec_secure(message, code: str, *, allow_imports=False, timeout=16,
                print_only=False, max_depth=4):
    if len(code.strip()) == 0:
        raise Exception('No code supplied!')

    # replace macros
    macros = get_macros(message.guild.id)
    code = string_utils.indented_replace_dict(code, macros, max_depth)

    # Wrap code in a function, so that we can use return
    # indent code
    code = '\n'.join(f'  {line}' for line in code.split('\n'))
    code = f'def __return_value_function__():\n{code}\n\n__return_value__[0] = __return_value_function__()'

    if print_only:
        return code

    # Wrapper functions for global functions
    class linksmodule:

        def add_links(links, insert_at=-1, verbose=False):
            return add_links(message, links, insert_at=insert_at, verbose=verbose)

        def remove_links(indices, verbose=False):
            return remove_links(message, indices, verbose=verbose)
    # ======================================

    ret = [None]
    import secure_utils

    try:
        func_timeout(
            timeout,
            lambda: secure_utils.exec_secure(
                code,
                globals={'__return_value__': ret},
                allowed_imports=allowed_imports,
                alternate_imports={
                    'links': linksmodule,
                    'requests': secure_utils.secure_requests
                },
                replace_import=(not allow_imports)
            )
        )
    except FunctionTimedOut as e:
        # print('Timed out!')
        print(timeout)
        print(e)
        return None
    except NameError as e:
        print(e)
        return None
    except Exception as e:
        print(e)
        return None
    except:
        print('Unknown Error!')
        return None

    return ret[0]


def get_macros(guild_id):
    rows = query_db(
        f'SELECT * FROM links WHERE guild = \'{guild_id}\' ORDER BY channel',
        fetch=True,
        verbose=False
    )
    macros = {}
    for row in rows:
        macname = row[1]
        if macname.startswith('@'):
            macros[macname] = row[3]
    return macros


def add_links(message, links, insert_at=-1, verbose=False):
    if len(links) == 0:
        'No Links supplied!'
    rows = query_db(
        'SELECT * FROM links WHERE guild = %s AND channel = %s ORDER BY line_id',
        data=(str(message.guild.id), str(message.channel.id)),
        fetch=True,
        verbose=verbose
    )
    print(f'Existing rows before insert: {len(rows)}')
    if insert_at < 0:
        insert_at = len(rows)
    if insert_at > len(rows):
        insert_at = len(rows)
    print(f'Insert at: {insert_at}')
    if insert_at == len(rows):
        print('Insert at end')
        sql = f'INSERT INTO links (guild, channel, line_id, line) VALUES '\
            + ','.join([
                f'(\'{message.guild.id}\', \'{message.channel.id}\', {insert_at + i}, %s)'
                for i, l in enumerate(links)
            ])
        query_db(
            sql,
            data=links,
            fetch=False,
            verbose=verbose
        )
    else:
        rows = [r[3] for r in rows]
        newLinks = rows[:insert_at] + links + rows[insert_at:]
        query_db(
            'DELETE FROM links WHERE guild = %s AND channel = %s',
            data=(str(message.guild.id), str(message.channel.id)),
            fetch=False,
            verbose=verbose
        )
        sql = f'INSERT INTO links (guild, channel, line_id, line) VALUES '\
            + ','.join([
                f'(\'{message.guild.id}\', \'{message.channel.id}\', {i}, %s)'
                for i, l in enumerate(newLinks)
            ])
        query_db(
            sql,
            data=newLinks,
            fetch=False,
            verbose=verbose
        )
    return 'Links updated.'

discord.channel
def remove_links(message, indices, verbose=False):
    rows = query_db(
        'SELECT * FROM links WHERE guild = %s AND channel = %s ORDER BY line_id',
        data=(str(message.guild.id), str(message.channel.id)),
        fetch=True,
        verbose=verbose
    )
    query_db(
        'DELETE FROM links WHERE guild = %s AND channel = %s',
        data=(str(message.guild.id), str(message.channel.id)),
        fetch=False,
        verbose=verbose
    )
    indices = [i for i in indices if i >= 0 and i < len(rows)]
    newLinks = [l[3] for i, l in enumerate(rows) if i not in indices]
    if len(newLinks) > 0:
        sql = f'INSERT INTO links (guild, channel, line_id, line) VALUES '\
            + ','.join([
                f'(\'{message.guild.id}\', \'{message.channel.id}\', {i}, %s)'
                for i, l in enumerate(newLinks)
            ])
        query_db(
            sql,
            data=newLinks,
            fetch=False,
            verbose=verbose
        )
    return 'Removed Links ({})'.format(
        ', '.join([str(i) for i in indices]))


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


@client.event
async def on_message(message: discord.Message):
    # we do not want the bot to reply to itself
    if message.author.id == client.user.id:
        return
    content = message.content
    if content.startswith(config.prefix):
        content = content[len(config.prefix):].strip()
    else:
        return

    lines = list(filter(lambda x: len(x) > 0, content.split('\n')))
    lines[0] = lines[0].strip()
    args = [word for word in lines[0].split(' ') if len(word) > 0]
    cmd = args[0]

    ismod = bool(set(
        map(lambda e: e.name, message.author.roles)).intersection(set(mods)))
    isadmin = bool(str(message.author) in admins)

    if cmd == 'info':
        return await message.channel.send(
            (
                'PythonBot v{0.version}\n'
                '> {0.description}'
            ).format(config)
        )

    if cmd == 'help':
        arg = args[1] if len(args) > 1 else None
        return await reply_msg(
            message,
            help(arg=arg)
        )

    elif cmd == 'links':
        if len(args) <= 1:
            rows = query_db(
                'SELECT * FROM links WHERE guild = \'{0.guild.id}\' AND channel = \'{0.channel.id}\' ORDER BY line_id'.format(
                    message),
                fetch=True,
                verbose=True
            )
            if len(rows) == 0:
                return await reply_msg(
                    message,
                    f'No links for channel {message.channel.mention}!'
                )
            else:
                return await reply_msg(
                    message,
                    'Links for channel {}\n{}'.format(
                        message.channel.mention,
                        '\n'.join(['> `[{}]` {}'.format(str(i).zfill(2), l)
                                   for g, c, i, l in rows])
                    )
                )
        else:
            operation = args[1].lower()
            if not ismod:
                return await reply_msg(
                    message,
                    'Sorry, you don\'t have permissions to use this!'
                )

            if operation == 'add':
                if len(lines) == 1:
                    return await reply_msg(
                        message,
                        'No Links supplied!'
                    )
                insert_at = -1
                if len(args) > 2:
                    try:
                        insert_at = eval_secure(
                            lines[0][len(f'{args[0]} {args[1]} '):]
                        )
                    except Exception as e:
                        print(e)
                        pass
                msg = add_links(
                    message, lines[1:], insert_at=insert_at, verbose=True)
                return await reply_msg(message, msg)

            elif operation == 'remove':
                if len(args) == 2:
                    return await reply_msg(
                        message,
                        'Please provide the indices of the links you want to remove! (No indices supplied)'
                    )
                expr = eval_secure(
                    lines[0][len(f'{args[0]} {args[1]} '):]
                )
                if isinstance(expr, int):
                    expr = [expr]
                try:
                    expr = [int(i) for i in expr]
                    if len(expr) == 0:
                        raise Exception
                except:
                    return await reply_msg(
                        message,
                        'Please provide the indices of the links you want to remove! (invalid indices)'
                    )
                msg = remove_links(message, expr, verbose=True)
                return await reply_msg(
                    message,
                    msg
                )
            else:
                return await reply_msg(
                    message,
                    f'"{operation}" is not a valid argument!'
                )

    elif cmd == 'exec':
        if not ismod:
            return await reply_msg(
                message,
                'Sorry, you don\'t have permissions to use this!'
            )

        indices = [m.start() for m in re.finditer('```', content)]
        if len(indices) >= 2:
            code = content[indices[0]+3:indices[1]]
            if code.startswith('python'):
                code = code[len('python'):]
            if code.startswith('py'):
                code = code[len('py'):]
            code = code.strip()
        else:
            code = '\n'.join(lines[1:])
        print(f'Executing\n{code}')

        arguments = {
            'timeout': 16, 'lib': False, 'print': False, 'modules': False,
            'refresh_rate': 1, 'max_lines': 32, 'max_depth': 4
        }
        for arg in args[1:]:
            argsplit = arg.split('=')
            if len(argsplit) > 1:
                arguments[argsplit[0]] = eval(argsplit[1])
            else:
                arguments[argsplit[0]] = True

        if arguments['lib'] == True:
            return await reply_msg(
                message,
                'Avaiable functions:\n```{}```'.format(
                    '\n'.join(
                        [f'- {doc.exec_desc[name][0]}' for name in doc.exec_desc]
                    )
                )
            )

        if arguments['modules'] == True:
            return await reply_msg(
                message,
                'Allowed imports:\n```{}```'.format(
                    '\n'.join(
                        [f'- {name}' for name in allowed_imports]
                    )
                ) +
                'Custom imports:\n```{}```'.format(
                    '\n'.join(
                        [f'- {name}' for name in alternate_imports]
                    )
                )
            )

        msg = None
        if arguments['print'] != True:
            msg = await message.channel.send(
                f'{message.author.mention}\n' +
                'Console Output:```---```'
            )

        max_lines = arguments['max_lines']
        if not isinstance(max_lines, int):
            max_lines = 32
        max_lines += 1

        max_depth = arguments['max_depth']
        if not isinstance(max_depth, int):
            max_depth = 4

        ret = [None]
        with stdoutIO() as s:
            try:
                def job():
                    try:
                        ret[0] = exec_secure(
                            message, code, allow_imports=isadmin,
                            timeout=arguments['timeout'],
                            print_only=(arguments['print'] == True),
                            max_depth=max_depth
                        )
                    except Exception as e:
                        print(e)
                        pass

                thread = threading.Thread(target=job)
                thread.start()
                while True:
                    refresh = arguments['refresh_rate']
                    if not isinstance(refresh, (int, float)):
                        refresh = 1
                    thread.join(timeout=(1 / refresh))
                    if not thread.is_alive():
                        break
                    output = s.getvalue()
                    if len(output.strip()) == 0:
                        output = '---'
                    output_lines = output.split('\n')
                    if len(output_lines) > max_lines:
                        start_index = max(len(output_lines) - max_lines, 0)
                        output = '\n'.join(output_lines[start_index:])
                    await msg.edit(
                        content=(
                            f'{message.author.mention}\n' +
                            f'Console Output:```\n{output[:len(output) - 1]}```'
                        )
                    )

            except Exception as e:
                print(e)
                pass

        print('Execution finished')

        if arguments['print'] == True:
            return await reply_msg(
                message,
                f'Code:\n```py\n{ret[0]}\n```'
            )
        output = s.getvalue()
        if len(output.strip()) == 0:
            output = '---'
        output_lines = output.split('\n')
        if len(output_lines) > max_lines:
            start_index = max(len(output_lines) - max_lines, 0)
            output = '\n'.join(output_lines[start_index:])
        return await msg.edit(
            content=(
                f'{message.author.mention}\n' +
                f'Console Output:```\n{output}```' +
                (f'Return Value:```\n{ret[0]}```' if ret[0] != None else '')
            )
        )

    elif cmd == 'def':
        if not ismod:
            return await reply_msg(
                message,
                'Sorry, you don\'t have permissions to use this!'
            )

        macros = get_macros(message.guild.id)
        if len(args) == 1:
            if len(macros) == 0:
                return await reply_msg(
                    message,
                    'No macros on this server!'
                )
            msg = ['- ' + name for name in macros]
            msg = '`==[ALL MACROS]==`\n```' + '\n'.join(msg) + '```'
            return await reply_msg(
                message,
                msg
            )
        else:
            if len(args) < 3:
                return await reply_msg(message, 'Please specify a name.')
            arg = args[1]
            name = args[2]
            if not name.startswith('@'):
                name = '@' + name

            if arg == 'show':
                rows = query_db(
                    'SELECT line FROM links WHERE guild = %s AND channel = %s AND line_id = 0',
                    data=(message.guild.id, name),
                    fetch=True,
                    verbose=True
                )
                if len(rows) == 0:
                    msg = f'No entry under the name "{name}"!'
                else:
                    msg = f'`{name}:`\n```python\n{rows[0][0]}\n```'
                return await reply_msg(
                    message,
                    msg
                )

            if arg == 'set':
                indices = [m.start() for m in re.finditer('```', content)]
                if len(indices) >= 2:
                    code = content[indices[0]+3:indices[1]]
                    if code.startswith('python'):
                        code = code[len('python'):]
                    if code.startswith('py'):
                        code = code[len('py'):]
                    code = code.strip()
                else:
                    code = '\n'.join(lines[1:])

                query_db(
                    'DELETE FROM links WHERE guild = %s AND channel = %s',
                    data=(message.guild.id, name),
                    fetch=False,
                    verbose=True
                )

                query_db(
                    'INSERT INTO links (guild, channel, line_id, line) VALUES (%s, %s, 0, %s)',
                    data=(message.guild.id, name, code),
                    fetch=False,
                    verbose=True
                )
                return await reply_msg(
                    message,
                    f'Set `{name}`!'
                )

            if arg == 'remove':
                query_db(
                    'DELETE FROM links WHERE guild = %s AND channel = %s',
                    data=(message.guild.id, name),
                    fetch=False,
                    verbose=True
                )
                return await reply_msg(
                    message,
                    f'Removed `{name}`!'
                )


if __name__ == '__main__':
    with open(os.path.expanduser('~/.discord-database')) as db_file:
        data_base = json.load(db_file)

    # Init database
    # create tables if necessary:
    query_db(
        'CREATE TABLE if not exists links ( guild varchar(255), channel varchar(255), line_id int, line varchar(2550) )',
        fetch=False,
        verbose=True
    )

    query_db(
        'CREATE TABLE if not exists macros ( guild varchar(255), name varchar(255), body varchar(4096) )',
        fetch=False,
        verbose=True
    )

    with open(os.path.expanduser('~/.discord-tokens')) as tokens_file:
        data = json.load(tokens_file)
        client.run(data['python-bot'])
