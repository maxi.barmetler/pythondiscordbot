cmd_desc = {
    'help': [
        'help <cmd> - info about specified command or list of commands'
    ],
    'links': [
        'links      - display links for this channel',
        '             add \\n <text>    - append link to list',
        '             remove <index>   - remove link at given index'
    ],
    'exec': [
        'exec       - execute python code (within markdown or starting at the second line)',
        '             timeout=<x>      - timeout in seconds, max 60, default 16',
        '             lib              - print all avaiable functions',
        '             modules          - list avaiable modules',
        '             print            - doesn\'t execute, only prints code',
        '             refresh_rate=<x> - how many times per second to update console',
        '             max_lines=<x>    - only print the last x lines',
        '             max_depth=<x>    - max recursion depth for macro replacement, default:4 '
    ],
    'def': [
        'def        - manage macros for $exec',
        '                              - list all macros',
        '             show <name>      - display specified macro',
        '             set <name>       - set macro for specified name',
        '             remove <name>    - remove macro'
    ]
}

exec_desc = {
    'add_links': [
        'add_links(links, insert_at=-1) from module \'links\'',
        'links      - an iterable containing the links to add',
        'insert_at  - index in existing links to insert links at, -1 is end',
        'verbose    - print more information',
        'returns      success'
    ],
    'remove_links': [
        'remove_links(indices)          from module \'links\'',
        'indices    - an iterable containing the indieces to remove',
        'verbose    - print more information',
        'returns      success'
    ]
}