from typing import (
    TypeVar, Iterator, Iterable, NoReturn, overload, Container,
    Sequence, MutableSequence, Mapping, MutableMapping, Tuple, List, Any, Dict, Callable, Generic,
    Set, AbstractSet, FrozenSet, MutableSet, Sized, Reversible, SupportsInt, SupportsFloat, SupportsAbs,
    SupportsComplex, IO, BinaryIO, Union,
    ItemsView, KeysView, ValuesView, ByteString, Optional, AnyStr, Type, Text,
)

import string_utils
import requests
import re
import builtins

realimport = builtins.__import__
verbose = False


def exec_secure(code: Text, *,
                globals: Dict[Text, Any] = {},
                locals: Dict[Text, Any] = {},
                allowed_imports: Iterable[Text] = [],
                alternate_imports: Dict[Text, Any] = {},
                replace_import: bool = True):

    import builtins
    oldimport = builtins.__import__

    def myimport(name: Text, globals: Optional[Mapping[str, Any]] = ...,
                 locals: Optional[Mapping[str, Any]] = ...,
                 fromlist: Sequence[str] = ...,
                 level: int = ...) -> Any:
        if name in alternate_imports:
            return alternate_imports[name]

        if name not in allowed_imports:
            raise Exception(f'Illegal import: {name}')
            return None

        return oldimport(name, globals=globals, locals=locals, fromlist=fromlist, level=level)

    if replace_import:
        builtins.__import__ = myimport

    if verbose:
        print(f'Executing:\n[[\n{string_utils.indent(code, 2)}\n]]')

    try:
        exec(code, globals, locals)
    except Exception as e:
        print(f'Exception:\n[[\n{string_utils.indent(str(e), 2)}\n]]')
        pass
    except:
        pass

    builtins.__import__ = oldimport


allowed_hosts = [
    'youtube.com',
    'home.in.tum.de/~barmetle'
]


class secure_requests:

    def get(url: Text, params: Dict[Any, Any] = {}, **kwargs):
        if not re.match(
            (
                r'^([a-zA-T])*(\:\/\/)([a-zA-Z0-9]+\.)*' +
                '({})'.format(
                    '|'.join(
                        re.escape(host) for host in allowed_hosts
                    )
                )
                + r'\/?.*'
            ),
            url
        ):
            raise Exception(f'Illegal url: {url}')
        __old_import__ = builtins.__import__
        builtins.__import__ = realimport
        r = requests.get(url, params=params, **kwargs)
        builtins.__import__ = __old_import__
        return r


if __name__ == '__main__':
    verbose = True

    exec_secure(
        (
            'print(\'asd\')\n'
            'from requests import get\n'
            'print(get(\'http://home.in.tum.de/~barmetle/lol\'))'
        ),
        allowed_imports=['math', 'numpy', 'time'],
        alternate_imports={'requests': secure_requests}
    )
    import os
